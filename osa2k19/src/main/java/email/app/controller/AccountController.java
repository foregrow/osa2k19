package email.app.controller;

import java.util.ArrayList;





import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import email.app.dto.AccountDTO;
import email.app.entity.Account;
import email.app.entity.Folder;
import email.app.entity.User;
import email.app.service.AccountService;
import email.app.service.FolderService;
import email.app.service.MessageService;
import email.app.service.UserService;
import email.app.util.LoggedInUser;


@Controller
@RequestMapping("/users/accounts")
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	FolderService folderService;
	
	
	@GetMapping
	public ResponseEntity<List<AccountDTO>> getAccounts() {
		List<Account> accs = accountService.findAll();
		
		
		
		List<AccountDTO> accsDTO = new ArrayList<AccountDTO>();
		for (Account a : accs) {
			if(a.getUser().getUsername().equals(LoggedInUser.userName)) {
				accsDTO.add(new AccountDTO(a));
			}		
		}
	
		return new ResponseEntity<List<AccountDTO>>(accsDTO, HttpStatus.OK);
	}
	
	@GetMapping("/addView")
	public static String view() {
		
		return "addaccount";
	}
	
	@PostMapping("/add")
	public ResponseEntity<Object> accountAdd(@RequestParam("displayName") String displayName,@RequestParam("userName") String userName,
			@RequestParam("password") String password){
		String obavestenje = "proslo";
		
		Account ac = accountService.findAccountByUsername(userName);
		
		if(displayName.equals("")) {
			obavestenje = "dNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);		
		}else if(userName.equals("")) {
			obavestenje = "uNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);	
		}else if(password.equals("")) {
			obavestenje = "pNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);	
		}else if(ac != null) {
			obavestenje = "aFound";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);	
		}
		
		User user = userService.findByUsername(LoggedInUser.userName);
		Account acc = new Account();
		acc.setDisplayName(displayName);
		acc.setPassword(password);
		acc.setUsername(userName);
		acc.setInServerAddress("imap.gmail.com");
		acc.setInServerPort(995);
		acc.setInServerType(1);
		acc.setSmtpAddress("smtp.gmail.com");
		acc.setSmtpPort(587);
		acc.setUser(user);
		accountService.save(acc);
		
		Folder f = new Folder();
		f.setName("Inbox");
		f.setAccount(acc);
		
		folderService.save(f);
		
		
		Folder f1 = new Folder();
		f1.setName("Sent");
		f1.setAccount(acc);
		
		folderService.save(f1);
		
		
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@GetMapping(value="/login/{id}")
	public String accountLogin() {
		return "accountlogin";
	}
	
	@GetMapping("/loginView")
	public ResponseEntity<Object> accLogin(@RequestParam("id") long id, @RequestParam("password") String password){
		User user = userService.findByUsername(LoggedInUser.userName);
		Account acc = null;
		String obavestenje = "prosao";
		for(Account a : user.getUserAccounts()) {
			if(a.getId() == id) {
				acc = accountService.findAccountById(id);
				break;
			}
		}
		
		if(acc == null) {
			obavestenje = "aNull";
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		
		
		if(!acc.getPassword().equals(password)) {
			obavestenje = "pBad";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		acc.setRequiredLogin(false);
		accountService.save(acc);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	
	

	@GetMapping(value="{id}/profile")
	public String getAcc() {
		return "accountprofile";
	}
	
	
	@PutMapping(value="/update")
	public ResponseEntity<Object> accUpdate(@RequestParam("password") String password, @RequestParam("id") long id){
		Account acc = accountService.findAccountById(id);
		String obavestenje = "proslo";
		
		if(password.equals("")) {
			obavestenje = "pNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		acc.setPassword(password);
		accountService.save(acc);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	

	@DeleteMapping(value="/delete/{id}")
	public ResponseEntity<Void> accDelete(@PathVariable("id") long id) {
	
		accountService.delete(id);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	

	
}
