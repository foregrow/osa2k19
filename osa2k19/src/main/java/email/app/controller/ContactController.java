package email.app.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import email.app.dto.ContactDTO;
import email.app.dto.PhotoDTO;
import email.app.entity.Contact;
import email.app.entity.Photo;
import email.app.entity.User;
import email.app.service.ContactService;
import email.app.service.PhotoService;
import email.app.service.UserService;
import email.app.util.LoggedInUser;


@Controller
@RequestMapping("/users/contacts")
public class ContactController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	PhotoService photoService;
	
	

	@GetMapping
	public ResponseEntity<List<ContactDTO>> getContacts() {
		List<Contact> contacts = contactService.findAll();
		
		List<ContactDTO> contactsDTO = new ArrayList<ContactDTO>();
		for (Contact c : contacts) {
			if(c.getUser().getUsername().equals(LoggedInUser.userName)) {
				contactsDTO.add(new ContactDTO(c));
			}
			

		}
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public String getContactById() {
		return "contact";
	}
	
	@GetMapping("/view")
	public ResponseEntity<Object> contactView(@RequestParam("id") long id){
		Contact c = contactService.findContactById(id);
		ContactDTO dto = new ContactDTO(c);
		List<Photo> photos = photoService.findAll();
		for(Photo p : photos) {
			if(p.getContact().getId() == id)
				dto.getContactPhotosDTO().add(new PhotoDTO(p));
		}
		
		return new ResponseEntity<Object>(dto,HttpStatus.OK);
	}
	
	@GetMapping("/addView")
	public static String view() {
		
		return "addcontact";
	}
	
	@PostMapping("/add")
	public ResponseEntity<Object> tagAdd(@RequestParam("displayName") String displayName,@RequestParam("email") String email,
			@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,@RequestParam("note") String note){
		String obavestenje = "proslo";
		if(displayName.equals("")) {
			obavestenje = "dNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);		
		}else if(email.equals("")) {
			obavestenje = "eNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);	
		}
		
		User user = userService.findByUsername(LoggedInUser.userName);
		Contact c = new Contact();
		c.setDisplayName(displayName);
		c.setEmail(email);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		c.setNote(note);
		c.setUser(user);
		
		contactService.save(c);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@PutMapping(value="/update")
	public ResponseEntity<Object> contactUpdate(@RequestParam("displayName") String displayName, @RequestParam("id") long id,
			@RequestParam("email") String email,@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,
			@RequestParam("note") String note){
		Contact c = contactService.findContactById(id);
		String obavestenje = "proslo";
		
		if(displayName.equals("")) {
			obavestenje = "dNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}else if(email.equals("")) {
			obavestenje = "eNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		c.setDisplayName(displayName);
		c.setEmail(email);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		c.setNote(note);
		
		//Photo photo = new Photo();
		//String path = System.getProperty("user.dir")+ "/data/" + id;
		//photo.setContact(c);
		//photo.setPath(path);
		//photoService.save(photo);
		contactService.save(c);
		
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@DeleteMapping(value="/delete/{id}")
	public ResponseEntity<Void> contactDelete(@PathVariable("id") long id) {
		System.out.println(id);
		contactService.delete(id);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	
	@PostMapping("/uploadPhoto/{id}")
	public String upload(Model model,@RequestParam("files") MultipartFile[] files,@PathVariable("id") long id) {
		
		Contact c = contactService.findContactById(id);
		String pp =  System.getProperty("user.dir")+ "/data/" + id;
		
		Path base = Paths.get(pp);
		if (!Files.exists(base)) {
			try {
				Files.createDirectory(base);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(id);
		

		StringBuilder fileNames = new StringBuilder();
		for(MultipartFile file : files) {
			Path fp = Paths.get(pp+"/",file.getOriginalFilename());
			fileNames.append(file.getOriginalFilename()+" ");
			try {
				Photo p = new Photo();
				p.setContact(c);
				p.setPath(pp+"/"+file.getOriginalFilename());
				c.getContactPhotos().add(p);
				photoService.save(p);
				System.out.println(p.getPath());
				
				Files.write(fp, file.getBytes());
			}catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return "contact";
		
	}
	
}
