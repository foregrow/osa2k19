package email.app.controller;

import java.util.ArrayList;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import email.app.dto.AccountDTO;
import email.app.dto.FolderDTO;
import email.app.dto.MessageDTO;
import email.app.entity.Account;

import email.app.entity.Folder;
import email.app.entity.Message;

import email.app.service.AccountService;
import email.app.service.FolderService;
import email.app.service.MessageService;



@Controller
@RequestMapping("/users/accounts")
public class FolderController {

	
	@Autowired
	FolderService folderService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	MessageService messageService;
	
	
	@GetMapping(value="/{id}/folders")
	public String getFolders() {
		return "folders";
	}
	
	@GetMapping("/folders/view")
	public ResponseEntity<List<FolderDTO>> getAccMessages(@RequestParam("id") long id) {
		Account acc = accountService.findAccountById(id);
		
		AccountDTO adto = new AccountDTO(acc);
		List<FolderDTO> fdto = new ArrayList<FolderDTO>();
		for(Folder f : acc.getAccountFolders()) {
			adto.getAccountFoldersDTO().add(new FolderDTO(f));
		}
		fdto = adto.getAccountFoldersDTO();
	
		return new ResponseEntity<List<FolderDTO>>(fdto, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}/folders/{fid}")
	public String getAcc() {
		return "folderView";
	}
	
	@PutMapping(value="/folders/update")
	public ResponseEntity<Object> folderUpdate(@RequestParam("name") String name, @RequestParam("id") long id){
		Folder f = folderService.findFolderById(id);
		String obavestenje = "proslo";
		
		if(name.equals("")) {
			obavestenje = "nNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		f.setName(name);
		folderService.save(f);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@DeleteMapping(value="/folders/delete/{id}")
	public ResponseEntity<Void> tagDelete(@PathVariable("id") long id) {
		folderService.delete(id);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	
	@GetMapping(value="/{id}/folders/{fid}/messages")
	public String getFolderMessages() {
		return "foldermessages";
	}
	
	@GetMapping("/folders/messages")
	public ResponseEntity<FolderDTO> getMessages(@RequestParam("id") long id){
		
		Folder f = folderService.findFolderById(id);
		FolderDTO dto = new FolderDTO(f);
		List<Message> messages = messageService.findAll();
		for(Message m : messages) {
			if(m.getFolder().getId() == id)
				dto.getfolderMessagesDTO().add(new MessageDTO(m));
		}
		for(Folder fo : f.getFolderFolders()) {
			System.out.println("foooo" + fo.getId());
			dto.getFolderFoldersDTO().add(new FolderDTO(fo));
			
		}
		System.out.println("dto +++" + dto.getFolder().getId());
		return new ResponseEntity<FolderDTO>(dto,HttpStatus.OK);
	}
	
	@GetMapping("{id}/folders/addView")
	public static String view() {
		
		return "addfolder";
	}
	
	@PostMapping("/folders/add")
	public ResponseEntity<Object> folderAdd(@RequestParam("name") String name,@RequestParam("parent") String parent, @RequestParam("id") long aid){
		String obavestenje = "proslo";
		Account acc = accountService.findAccountById(aid);
		Folder fParent = folderService.findByNameAndAccount(parent, acc);
		if(name.equals("")) {
			obavestenje = "nNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);		
		}
		
		if(!parent.equals("")) {
			if(fParent == null) {
				obavestenje = "pNull";
				return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);	
			}
		}
		if(name.equals("Inbox") || name.equals("Sent")) {
			obavestenje = "isNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		if(parent.equals("Inbox") || parent.equals("Sent")) {
			obavestenje = "pisNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		Folder fnew = new Folder();
		fnew.setName(name);
		fnew.setFolder(fParent);
		fnew.setAccount(acc);
		
		folderService.save(fnew);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
}
