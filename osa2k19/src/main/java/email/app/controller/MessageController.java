package email.app.controller;

import java.sql.Timestamp;



import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import email.app.dto.AccountDTO;
import email.app.dto.MessageDTO;
import email.app.dto.TagDTO;
import email.app.entity.Account;

import email.app.entity.Folder;
import email.app.entity.Message;
import email.app.entity.Tag;
import email.app.entity.User;
import email.app.mailAPI.MailApiCfg;
import email.app.service.AccountService;
import email.app.service.FolderService;
import email.app.service.MessageService;
import email.app.service.TagService;
import email.app.service.UserService;
import email.app.util.LoggedInUser;

@Controller
@RequestMapping("/users/accounts")
public class MessageController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	FolderService folderService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TagService tagService;
	
	@GetMapping("/{id}/messages")
	public String getAccountById() {
		return "account";
	}
	
	@GetMapping("/messages/view")
	public ResponseEntity<List<MessageDTO>> getAccMessagesInbox(@RequestParam("id") long id,@RequestParam("sort") int sort,
			@RequestParam("search") String search, @RequestParam("searchInt") int searchInt,
			@RequestParam("tagSearch") String tagSearch, @RequestParam("tagSearchInt") int tagSearchInt) {
		Account acc = accountService.findAccountById(id);
		
		List<MessageDTO> mdto = new ArrayList<MessageDTO>();	
	
		List<Message> allMails = null;
		
		if(searchInt == 0) {
			if(sort == -1) {
				Folder fol = folderService.findByNameAndAccount("Inbox", acc);
				
				List<Message> forSaving = MailApiCfg.loadEmails(acc,fol);
				if(!forSaving.isEmpty()) {
					for(Message m : forSaving) {
						if(messageService.findByDateTimeAndAccountIdAndFolderId(m.getDateTime(), m.getAccount().getId(),m.getFolder().getId()) == null) {
							messageService.save(m);
						}
					}
				}
				allMails = messageService.findAll();
					
				if(allMails.isEmpty()) {
					System.out.println("allmails");
				}

				AccountDTO adto = new AccountDTO(acc);
				
				for(Message m : allMails) {
					if(m.getAccount().getId() == acc.getId()) {
						adto.getAccountMessagesDTO().add(new MessageDTO(m));
					}
				}

				mdto = adto.getAccountMessagesDTO();
				return new ResponseEntity<List<MessageDTO>>(mdto, HttpStatus.OK);
			}
		}
		
		
		AccountDTO adto = new AccountDTO(acc);
		
		//allMails = messageService.findAll();
		List<Message> forFiltering = messageService.findAll();
		allMails = new ArrayList<>();
		if(!tagSearch.equals("prazan") && !tagSearch.equals("")) {
			for(Message me : forFiltering) {
				for(Tag t : me.getTags()) {
					if(t.getName().equals(tagSearch)) {
						allMails.add(me);
					}
				}
			}
		}
		
		

		if(!search.equals("prazan") && !search.equals("")) {
			if(tagSearch.equals("prazan") || tagSearch.equals("")) {
				allMails = messageService.findAll();
			}
			allMails= allMails.stream().filter(mm -> mm.getDateTime().toString().contains(search) || mm.getFrom().contains(search) 
					|| mm.getTo().contains(search) || mm.getSubject().contains(search)).collect(Collectors.toList());
		}
		
		if(search.equals("") && tagSearch.equals("")) {
			allMails = messageService.findAll();
		}
		
		for(Message m : allMails) {
			if(m.getAccount().getId() == acc.getId()) {
				adto.getAccountMessagesDTO().add(new MessageDTO(m));
			}
		}
			
		mdto = adto.getAccountMessagesDTO();
		
		switch(sort) {
			case 1:
				mdto.sort(Comparator.comparing(MessageDTO :: getDateTime));		
				break;
			case 2:
				mdto.sort(Comparator.comparing(MessageDTO :: getDateTime).reversed());
				break;
			case 3:
				mdto.sort(Comparator.comparing(MessageDTO :: getFrom));
				break;
			case 4:
				mdto.sort(Comparator.comparing(MessageDTO :: getFrom).reversed());
				break;
			case 5:
				mdto.sort(Comparator.comparing(MessageDTO :: getSubject));
				break;
			case 6:
				mdto.sort(Comparator.comparing(MessageDTO :: getSubject).reversed());
				break;
			default:
				break;
		}

		return new ResponseEntity<List<MessageDTO>>(mdto, HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}/messages/{mid}")
	public String getMessageById() {
		
		return "message";
	}
	
	@GetMapping("/{id}/messages/messageView")
	public ResponseEntity<?> getMessageView(@RequestParam("id") long mid) {
		MessageDTO dto = null;
		
		if(Long.toString(mid) != null && !Long.toString(mid).equals("")) {
			Message mess = messageService.findById(mid);
			
			mess.setUnread(false);
			messageService.save(mess);
			dto = new MessageDTO(mess);
			
			for(Tag tag : mess.getTags()) {
				dto.getTags().add(new TagDTO(tag));
			}
		}
		
		return new ResponseEntity<Object>(dto, HttpStatus.OK);
	}
	
	@GetMapping("/{id}/messages/createMessageView")
	public String newMessageView() {
		return "sendmessage";
	}
	
	@PostMapping("/messages/createMessage")
	public ResponseEntity<Object> createMessage(@RequestParam("id") long accId, @RequestParam("to") String to, @RequestParam("cc") String cc,
			@RequestParam("bcc") String bcc, @RequestParam("content") String content, @RequestParam("subject") String subject){
		
		String obavestenje = "proslo";
		if(Long.toString(accId) == null || Long.toString(accId).equals("")) {
			obavestenje = "aNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK); 
		}
		Account acc = accountService.findAccountById(accId);
		
		
		MailApiCfg.sendEmail(acc.getUsername(), to, cc, bcc, acc.getSmtpAddress(), acc.getSmtpPort().toString(), acc.getPassword(), subject, content);
		
		Date date= new Date();
	    Timestamp timestamp = new Timestamp(date.getTime());
		
		Message mess = new Message();
		mess.setAccount(acc);
		mess.setContent(content);
		mess.setDateTime(timestamp);
		mess.setTo(to);
		mess.setSubject(subject);
		mess.setFrom(acc.getUsername());
		mess.setCc(cc);
		mess.setBcc(bcc);
		mess.setUnread(false);
		for(Folder f : acc.getAccountFolders()) {
			if(f.getName().equals("Sent")) {
				mess.setFolder(f);
				messageService.save(mess);
				break;
			}
		}
		return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
	}
	
	@PostMapping("{id}/messages/addTag")
	public ResponseEntity<Object> addTagToMess(@RequestParam("tagName") String tagName, @RequestParam("id") long mid){
		String obavestenje = "proslo";
		
		if(Long.toString(mid).equals("") || Long.toString(mid) == null) {
			obavestenje = "mNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		User u = null;
		u = userService.findByUsername(LoggedInUser.userName);
		if(u == null) {
			obavestenje = "uNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		
		Tag tag = tagService.findByUserAndName(u, tagName);
			
		if(tag == null) {
			obavestenje = "tNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		
		Message mess = messageService.findById(mid);
		mess.getTags().add(tag);
		messageService.save(mess);
			
		return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
	}
	
	@PostMapping("{id}/messages/addFolder")
	public ResponseEntity<Object> addMessToFolder(@RequestParam("folderName") String folderName, @RequestParam("id") long mid, @RequestParam("aid") long aid){
		String obavestenje = "proslo";
		if(Long.toString(mid).equals("") || Long.toString(mid) == null || Long.toString(aid).equals("") || Long.toString(aid) == null) {
			obavestenje = "mNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		if(folderName.equals("")) {
			obavestenje = "fNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		Account acc = accountService.findAccountById(aid);
		Folder accFolder = folderService.findByNameAndAccount(folderName, acc);
		Message message = messageService.findById(mid);
		
		if(accFolder == null) {
			obavestenje = "afNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		
		if(accFolder.getName().equals("Sent") || accFolder.getName().equals("Inbox")) {
			obavestenje = "isNull";
			return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
		}
		
		if(message.getFolder().getName().equals("Sent") || message.getFolder().getName().equals("Inbox")) {
			Message mess = new Message();
			mess.setAccount(message.getAccount());
			mess.setBcc(message.getBcc());
			mess.setCc(message.getCc());
			mess.setContent(message.getContent());
			mess.setDateTime(message.getDateTime());
			mess.setFrom(message.getFrom());
			mess.setSubject(message.getSubject());
			mess.setTo(message.getTo());
			mess.setUnread(message.isUnread());
			mess.setFolder(accFolder);
			
			messageService.save(mess);
		}else if(!message.getFolder().getName().equals("Sent") && !message.getFolder().getName().equals("Inbox")) {
			message.setFolder(accFolder);
			
			messageService.save(message);
		}
		
		return new ResponseEntity<Object>(obavestenje, HttpStatus.OK);
	}
	

}
