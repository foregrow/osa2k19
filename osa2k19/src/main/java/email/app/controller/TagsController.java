package email.app.controller;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import email.app.dto.TagDTO;

import email.app.entity.Tag;
import email.app.entity.User;
import email.app.service.TagService;
import email.app.service.UserService;
import email.app.util.LoggedInUser;

@Controller
@RequestMapping("/users/tags")
public class TagsController {

	@Autowired
	TagService tagService;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public ResponseEntity<List<TagDTO>> getTags() {
		List<Tag> tags = tagService.findAll();
		
		
		
		List<TagDTO> tagsDTO = new ArrayList<TagDTO>();
		for (Tag t : tags) {
			if(t.getUser().getUsername().equals(LoggedInUser.userName)) {
				tagsDTO.add(new TagDTO(t));
			}		
		}
	
		return new ResponseEntity<List<TagDTO>>(tagsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public String getTagById() {
		return "tag";
	}
	
	@GetMapping("/view")
	public ResponseEntity<Object> tagView(@RequestParam("id") long id){
		Tag tag = tagService.findTagById(id);
		TagDTO dto = new TagDTO(tag);
		
		return new ResponseEntity<Object>(dto,HttpStatus.OK);
	}
	
	@GetMapping("/addView")
	public static String addView() {
		return "addtag";
	}
	
	@PostMapping("/add")
	public ResponseEntity<Object> tagAdd(@RequestParam("name") String name){
		String obavestenje = "proslo";
		if(name.equals("")) {
			obavestenje = "nNull";
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
			
		}
		User user = userService.findByUsername(LoggedInUser.userName);
		Tag tag = new Tag();
		tag.setName(name);
		tag.setUser(user);
		tagService.save(tag);
		
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@PutMapping(value="/update")
	public ResponseEntity<Object> tagUpdate(@RequestParam("name") String name, @RequestParam("id") long id){
		Tag tag = tagService.findTagById(id);
		String obavestenje = "proslo";
		
		if(name.equals("")) {
			obavestenje = "nNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		tag.setName(name);
		tagService.save(tag);
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
	}
	
	@DeleteMapping(value="/delete/{id}")
	public ResponseEntity<Void> tagDelete(@PathVariable("id") long id) {
		System.out.println(id);
		tagService.delete(id);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
}
