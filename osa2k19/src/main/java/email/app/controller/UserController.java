package email.app.controller;

import java.util.ArrayList;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import email.app.config.JwtTokenUtil;
import email.app.dto.UserDTO;

import email.app.entity.User;
import email.app.service.AccountService;
import email.app.service.JwtUserDetailsService;
import email.app.service.UserService;
import email.app.util.LoggedInUser;

@Controller
@RequestMapping 
@CrossOrigin
public class UserController {


	@Autowired
	UserService userService;

	@Autowired
	AccountService accountService;
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@GetMapping("/login")
	public static String login() {
		LoggedInUser.userName = "";
		LoggedInUser.password = "";
		LoggedInUser.token = "";
		return "login";
	}
	
	/*@GetMapping("/logout")
	public static String logout() {
		//LoggedInUser.userName = "";
		//LoggedInUser.password = "";
		//LoggedInUser.token = "";
		return "login";
	}*/
	
	@GetMapping("/home")
	public static String home() {
		return "home";
	}
	
	

	
	@GetMapping("/register")
	public static String register() {
		return "register";
	}
	

	@PostMapping("/reg")
	public ResponseEntity<Object> reg(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("userName") String userName,
			@RequestParam("password") String password) {
		//OVDE SE PISE FUNKCIJA ZA REGISTER KORISNIKA I OVO SE GADJA PRI SUBMITU IZ HTML FAJLA
		System.out.println("REGISTER");
		System.out.println("FIRSTNAME"+firstName);
		System.out.println("lastName"+lastName);
		System.out.println("userName"+userName);
		System.out.println("password"+password);
		String suser = "nevalidno";
		List<User> users = userService.findAll();
		
		List<UserDTO> dto = new ArrayList<>();
		for(User u : users) {
			dto.add(new UserDTO(u));
		}
		
		if(userName == null) {
			System.out.println("userName == null");
			return new ResponseEntity<Object>(suser,HttpStatus.OK);
		}
		
		boolean postoji = false;	
		for(UserDTO d : dto) {
			if(userName.equals(d.getUsername())){
				postoji = true;
				break;
			}
		}
		
		
		User user = new User();
		
		if(firstName.equals("") || lastName.equals("") || userName.equals("") || password.equals("")) {
			System.out.println("neki je prazan");
			return new ResponseEntity<Object>(suser,HttpStatus.OK);
		}else if(postoji) {
			System.out.println("vec postoji");
			return new ResponseEntity<Object>(suser,HttpStatus.OK);
		}else {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setUsername(userName);
			user.setPassword(password);
			System.out.println(user.getFirstName());
			System.out.println(user.getLastName());
			System.out.println(user.getUsername());
			System.out.println(user.getPassword());
			userService.save(user);
			suser = user.getUsername();
			return new ResponseEntity<Object>(suser,HttpStatus.OK);
		}
		
	}
	
	@GetMapping("/users/profile")
	public static String profile(){	
		return "profile";
	}
	
	@GetMapping("/users/profileView")
	public ResponseEntity<Object> profileView(){

		User user = userService.findByUsername(LoggedInUser.userName);
		UserDTO dto = new UserDTO(user);
		
		return new ResponseEntity<Object>(dto,HttpStatus.OK);
	}
	
	@PutMapping(value="/users/updateProfile")
	public ResponseEntity<Object> profileUpdate(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("userName") String userName,@RequestParam("password") String password){
		User user = userService.findByUsername(LoggedInUser.userName);
		List<User> users = userService.findAll();
		List<String> userUsernames = new ArrayList<>();
		System.out.println("yserName+" + user.getUsername());
		String obavestenje = "proslo";
		for(User u : users) {
			userUsernames.add(u.getUsername());
		}
		if(!userName.equals(LoggedInUser.userName)) {
			if(userUsernames.contains(userName)) {
				obavestenje = "uPostoji";
				System.out.println(obavestenje);
				return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
			}
		}
		
		if(firstName.equals("") || firstName==null) {
			
			obavestenje = "fNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		else if(lastName.equals("") || lastName==null) {
			obavestenje = "lNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		else if(password.equals("") || password==null) {
			obavestenje = "pNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		else if(userName.equals("") || userName==null) {
			obavestenje = "uNull";
			System.out.println(obavestenje);
			return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		}
		
		
		System.out.println(obavestenje);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUsername(userName);
		user.setPassword(password);
		LoggedInUser.userName = user.getUsername();
		
		userService.save(user);
		
		return new ResponseEntity<Object>(obavestenje,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestParam("userName") String userName, @RequestParam("password") String password) throws Exception {
		
		String token = "";
		User user = null;
		user = userService.findByUsernameAndPassword(userName, password);
		
		UserDTO dto = null;
		if(user != null) {
			dto = new UserDTO(user);
			LoggedInUser.userName = dto.getUsername();
			
			authenticate(dto.getUsername(), dto.getPassword());

			final UserDetails userDetails = userDetailsService.loadUserByUsername(dto.getUsername());

			token = jwtTokenUtil.generateToken(userDetails);
			LoggedInUser.token = token;
			dto.setToken(token);
			System.out.println("USERNAME: ++++++++++++++" + user.getUsername());
			System.out.println("password: ++++++++++++++" + user.getPassword());
			
			
		}
		System.out.println("TOKEN++" + token);
		return new ResponseEntity<Object>(dto,HttpStatus.OK);
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
