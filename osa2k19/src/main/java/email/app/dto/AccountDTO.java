package email.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import email.app.entity.Account;


@SuppressWarnings("serial")
public class AccountDTO implements Serializable {

	private long id;
	private String smtpAddress;
	private Integer smtpPort;
	private int inServerType;
	private String inServerAddress;
	private Integer inServerPort;
	private String username;
	private String password;
	private String displayName;
	private boolean requiredLogin = true;
	
	private UserDTO user;
	
	private List<MessageDTO> accountMessagesDTO = new ArrayList<MessageDTO>();
	
	private List<FolderDTO> accountFoldersDTO = new ArrayList<FolderDTO>();
	
	public AccountDTO() {
		
	}

	public AccountDTO(long id, String smtpAddress, Integer smtpPort, int inServerType, String inServerAddress,
			Integer inServerPort, String username, String password, String displayName, boolean requiredLogin, UserDTO user
			) {
		super();
		this.id = id;
		this.smtpAddress = smtpAddress;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.inServerAddress = inServerAddress;
		this.inServerPort = inServerPort;
		this.username = username;
		this.password = password;
		this.displayName = displayName;
		this.user = user;
		this.requiredLogin = requiredLogin;
	}
	
	public AccountDTO(Account a) {
		this(a.getId(),a.getSmtpAddress(),a.getSmtpPort(),a.getInServerType(),a.getInServerAddress(),a.getInServerPort(),a.getUsername(),a.getPassword(),
				a.getDisplayName(), a.isRequiredLogin(), (a.getUser()!= null)?new UserDTO(a.getUser()):new UserDTO());
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}

	public void setSmtpAddress(String smtpAddress) {
		this.smtpAddress = smtpAddress;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public int getInServerType() {
		return inServerType;
	}

	public void setInServerType(int inServerType) {
		this.inServerType = inServerType;
	}

	public String getInServerAddress() {
		return inServerAddress;
	}

	public void setInServerAddress(String inServerAddress) {
		this.inServerAddress = inServerAddress;
	}

	public Integer getInServerPort() {
		return inServerPort;
	}

	public void setInServerPort(Integer inServerPort) {
		this.inServerPort = inServerPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public boolean isRequiredLogin() {
		return requiredLogin;
	}

	public void setRequiredLogin(boolean requiredLogin) {
		this.requiredLogin = requiredLogin;
	}

	public List<MessageDTO> getAccountMessagesDTO() {
		return accountMessagesDTO;
	}

	public void setAccountMessagesDTO(List<MessageDTO> accountMessagesDTO) {
		this.accountMessagesDTO = accountMessagesDTO;
	}

	public List<FolderDTO> getAccountFoldersDTO() {
		return accountFoldersDTO;
	}

	public void setAccountFoldersDTO(List<FolderDTO> accountFoldersDTO) {
		this.accountFoldersDTO = accountFoldersDTO;
	}

	
		
}
