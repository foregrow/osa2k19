package email.app.dto;

import java.io.Serializable;
import java.util.Base64;

@SuppressWarnings("serial")
public class AttachmentDTO implements Serializable {
	
	private long id;
	private Base64 data;
	private String mime_type;
	private String name;
	
	private MessageDTO message;
	
	public AttachmentDTO() {
		
	}

	public AttachmentDTO(long id, Base64 data, String mime_type, String name, MessageDTO message) {
		super();
		this.id = id;
		this.data = data;
		this.mime_type = mime_type;
		this.name = name;
		this.message = message;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Base64 getData() {
		return data;
	}

	public void setData(Base64 data) {
		this.data = data;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MessageDTO getMessage() {
		return message;
	}

	public void setMessage(MessageDTO message) {
		this.message = message;
	}
	
	

}
