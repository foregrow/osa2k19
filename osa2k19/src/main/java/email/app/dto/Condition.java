package email.app.dto;

public enum Condition {

	TO,
	FROM,
	CC,
	BCC
}
