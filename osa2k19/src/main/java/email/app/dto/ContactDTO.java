package email.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import email.app.entity.Contact;



@SuppressWarnings("serial")
public class ContactDTO implements Serializable {
	
	private long id;
	private String firstName;
	private String lastName;
	private String displayName;
	private String email;
	private String note;
	
	private UserDTO user;
	private List<PhotoDTO> contactPhotosDTO = new ArrayList<PhotoDTO>();
	
	
	public ContactDTO() {
		
	}

	public ContactDTO(long id, String firstName, String lastName, String displayName, String email, String note,
			UserDTO user) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.displayName = displayName;
		this.email = email;
		this.note = note;
		this.user = user;
	}


	public ContactDTO(Contact c) {
		this(c.getId(),c.getFirstName(),c.getLastName(),c.getDisplayName(),c.getEmail(),c.getNote(),
				(c.getUser()!= null)?new UserDTO(c.getUser()):new UserDTO());
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public List<PhotoDTO> getContactPhotosDTO() {
		return contactPhotosDTO;
	}

	public void setContactPhotosDTO(List<PhotoDTO> contactPhotosDTO) {
		this.contactPhotosDTO = contactPhotosDTO;
	}
	
	
	
	

}
