package email.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import email.app.entity.Folder;


@SuppressWarnings("serial")
public class FolderDTO implements Serializable {

	private long id;
	private String name;
	
	private FolderDTO folder;
	private AccountDTO account;
	
	private List<MessageDTO> folderMessagesDTO = new ArrayList<MessageDTO>();
	private List<FolderDTO> folderFoldersDTO = new ArrayList<FolderDTO>();
	
	public FolderDTO() {
		
	}

	public FolderDTO(long id, String name, FolderDTO folder,
			AccountDTO account) {
		super();
		this.id = id;
		this.name = name;
		this.folder = folder;
		this.account = account;
	}
	
	public FolderDTO(Folder f) {
		this(f.getId(),f.getName(),
				(f.getFolder()!= null)?new FolderDTO(f.getFolder()):new FolderDTO(),
						(f.getAccount()!= null)?new AccountDTO(f.getAccount()):new AccountDTO());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FolderDTO getFolder() {
		return folder;
	}

	public void setFolder(FolderDTO folder) {
		this.folder = folder;
	}


	public AccountDTO getAccount() {
		return account;
	}

	public void setAccount(AccountDTO account) {
		this.account = account;
	}

	public List<MessageDTO> getfolderMessagesDTO() {
		return folderMessagesDTO;
	}

	public void setfolderMessagesDTO(List<MessageDTO> folderMessagesDTO) {
		this.folderMessagesDTO = folderMessagesDTO;
	}


	public List<FolderDTO> getFolderFoldersDTO() {
		return folderFoldersDTO;
	}

	public void setFolderFoldersDTO(List<FolderDTO> folderFoldersDTO) {
		this.folderFoldersDTO = folderFoldersDTO;
	}

	
	
	
	
}
