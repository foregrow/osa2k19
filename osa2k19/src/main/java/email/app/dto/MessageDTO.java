package email.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import email.app.entity.Message;


@SuppressWarnings("serial")
public class MessageDTO implements Serializable {

	private long id;
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private Date dateTime;
	private String subject;
	private String content;
	private boolean unread;
	
	private AccountDTO account;
	
	private FolderDTO folder;
	
	private List<TagDTO> tags = new ArrayList<TagDTO>();
	
	public MessageDTO() {
		
	}
	
	

	public MessageDTO(long id, String from, String to, String cc, String bcc, Date dateTime, String subject,
			String content, boolean unread, AccountDTO account,
			FolderDTO folder) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.unread = unread;
		this.account = account;
		this.folder = folder;
	}
	
	public MessageDTO(Message m) {
		this(m.getId(),m.getFrom(),m.getTo(),m.getCc(),m.getBcc(),m.getDateTime(),m.getSubject(),m.getContent(),m.isUnread(),
				(m.getAccount()!= null)?new AccountDTO(m.getAccount()):new AccountDTO(), 
						(m.getFolder()!= null)?new FolderDTO(m.getFolder()):new FolderDTO());
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	public AccountDTO getAccount() {
		return account;
	}

	public void setAccount(AccountDTO account) {
		this.account = account;
	}


	public FolderDTO getFolder() {
		return folder;
	}

	public void setFolder(FolderDTO folder) {
		this.folder = folder;
	}



	public List<TagDTO> getTags() {
		return tags;
	}



	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}
	
	
	
	
}
