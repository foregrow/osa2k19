package email.app.dto;

public enum Operation {

	MOVE,
	COPY,
	DELETE
}
