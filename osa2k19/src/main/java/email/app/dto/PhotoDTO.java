package email.app.dto;

import java.io.Serializable;

import email.app.entity.Photo;

@SuppressWarnings("serial")
public class PhotoDTO implements Serializable {

	private long id;
	private String path;
	
	private ContactDTO contact;
	
	public PhotoDTO() {
		
	}

	public PhotoDTO(long id, String path, ContactDTO contact) {
		super();
		this.id = id;
		this.path = path;
		this.contact = contact;
	}
	
	public PhotoDTO(Photo p) {
		this(p.getId(),p.getPath(),
				(p.getContact()!= null)?new ContactDTO(p.getContact()):new ContactDTO());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ContactDTO getContact() {
		return contact;
	}

	public void setContact(ContactDTO contact) {
		this.contact = contact;
	}
	
	
}
