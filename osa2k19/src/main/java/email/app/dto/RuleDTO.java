package email.app.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RuleDTO implements Serializable {
	
	private long id;
	private Condition condition;
	private Operation operation;
	private String value;
	
	private FolderDTO folder;
	
	public RuleDTO() {
		
	}

	public RuleDTO(long id, Condition condition, Operation operation, String value, FolderDTO folder) {
		super();
		this.id = id;
		this.condition = condition;
		this.operation = operation;
		this.value = value;
		this.folder = folder;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public FolderDTO getFolder() {
		return folder;
	}

	public void setFolder(FolderDTO folder) {
		this.folder = folder;
	}
	
	
	
	

}
