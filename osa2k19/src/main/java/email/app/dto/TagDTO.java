package email.app.dto;

import java.io.Serializable;


import email.app.entity.Tag;


@SuppressWarnings("serial")
public class TagDTO implements Serializable {
	
	private long id;
	private String name;
	
	private UserDTO user;
	
	public TagDTO() {
		
	}

	

	public TagDTO(long id, String name, UserDTO user) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
	}
	
	public TagDTO(Tag a) {
		this(a.getId(),a.getName(),(a.getUser()!= null)?new UserDTO(a.getUser()):new UserDTO());
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}



	
	
	
	

}
