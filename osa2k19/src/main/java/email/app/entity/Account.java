package email.app.entity;

import static javax.persistence.CascadeType.ALL;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@SuppressWarnings("serial")
@Entity
@Table(name="accounts")
public class Account implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="a_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="a_smtpAddress", unique=false, nullable=true)
	private String smtpAddress;
	
	@Column(name="a_smtpPort", unique=false, nullable=true)
	private Integer smtpPort;
	
	@Column(name="a_inServerType", unique=false, nullable=true)
	private int inServerType;
	
	@Column(name="a_inServerAddress", unique=false, nullable=true)
	private String inServerAddress;
	
	@Column(name="a_inServerPort", unique=false, nullable=true)
	private Integer inServerPort;
	
	@Column(name="a_username", unique=true, nullable=false)
	private String username;
	
	@Column(name="a_password", unique=false, nullable=false)
	private String password;
	
	@Column(name="a_displayName", unique=false, nullable=true)
	private String displayName;
	
	@Column(name="a_requiredLogin", unique=false, nullable=false)
	private boolean requiredLogin = true;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="u_id", nullable=false)
	private User user;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="account")
	private Set<Folder> accountFolders = new HashSet<Folder>();
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="account")
	private Set<Message> accountMessages = new HashSet<Message>();
	
	public Account() {
		
	}

	public Account(long id, String smtpAddress, Integer smtpPort, int inServerType, String inServerAddress,
			Integer inServerPort, String username, String password, String displayName, boolean requiredLogin, User user,
			Set<Folder> accountFolders, Set<Message> accountMessages) {
		super();
		this.id = id;
		this.smtpAddress = smtpAddress;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.inServerAddress = inServerAddress;
		this.inServerPort = inServerPort;
		this.username = username;
		this.password = password;
		this.displayName = displayName;
		this.user = user;
		this.accountFolders = accountFolders;
		this.accountMessages = accountMessages;
		this.requiredLogin = requiredLogin;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}

	public void setSmtpAddress(String smtpAddress) {
		this.smtpAddress = smtpAddress;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public int getInServerType() {
		return inServerType;
	}

	public void setInServerType(int inServerType) {
		this.inServerType = inServerType;
	}

	public String getInServerAddress() {
		return inServerAddress;
	}

	public void setInServerAddress(String inServerAddress) {
		this.inServerAddress = inServerAddress;
	}

	public Integer getInServerPort() {
		return inServerPort;
	}

	public void setInServerPort(Integer inServerPort) {
		this.inServerPort = inServerPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Folder> getAccountFolders() {
		return accountFolders;
	}

	public void setAccountFolders(Set<Folder> accountFolders) {
		this.accountFolders = accountFolders;
	}

	public Set<Message> getAccountMessages() {
		return accountMessages;
	}

	public void setAccountMessages(Set<Message> accountMessages) {
		this.accountMessages = accountMessages;
	}
	
	public boolean isRequiredLogin() {
		return requiredLogin;
	}

	public void setRequiredLogin(boolean requiredLogin) {
		this.requiredLogin = requiredLogin;
	}
	
	
}
