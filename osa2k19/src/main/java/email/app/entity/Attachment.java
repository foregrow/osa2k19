package email.app.entity;

import static javax.persistence.GenerationType.IDENTITY;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="attachments")
public class Attachment implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="a_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="a_data", unique=true, nullable=true) 
	private String data;
	
	@Column(name="a_mime_type", unique=true, nullable=false) 
	private String mime_type;
	
	@Column(name="a_name", unique=true, nullable=false) 
	private String name;
	
	@ManyToOne
	@JoinColumn(name="message_id", referencedColumnName="m_id", nullable=false)
	private Message message;
	
	public Attachment() {
		
	}

	public Attachment(long id, String data, String mime_type, String name, Message message) {
		super();
		this.id = id;
		this.data = data;
		this.mime_type = mime_type;
		this.name = name;
		this.message = message;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
}
