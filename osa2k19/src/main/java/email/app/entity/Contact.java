package email.app.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="contacts")
public class Contact implements Serializable {
	
	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="c_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="c_firstName", unique=true, nullable=false) 
	private String firstName;
	
	@Column(name="c_lastName", unique=true, nullable=false) 
	private String lastName;
	
	@Column(name="c_displayName", unique=true, nullable=true) 
	private String displayName;
	
	@Column(name="c_email", unique=true, nullable=false) 
	private String email;
	
	@Column(name="c_note", unique=true, nullable=true) 
	private String note;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="u_id", nullable=false)
	private User user;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="contact")
	private Set<Photo> contactPhotos = new HashSet<Photo>();
	
	public Contact() {
		
	}

	public Contact(long id, String firstName, String lastName, String displayName, String email, String note,
			User user, Set<Photo> contactPhotos) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.displayName = displayName;
		this.email = email;
		this.note = note;
		this.user = user;
		this.contactPhotos = contactPhotos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Photo> getContactPhotos() {
		return contactPhotos;
	}

	public void setContactPhotos(Set<Photo> contactPhotos) {
		this.contactPhotos = contactPhotos;
	}
}
