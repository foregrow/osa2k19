package email.app.entity;

import static javax.persistence.CascadeType.ALL;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="folders")
public class Folder implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="f_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="f_name", unique=false, nullable=false) 
	private String name;
	
	@ManyToOne
	@JoinColumn(name="parent_f_id", referencedColumnName="f_id", nullable=true)
	private Folder parent;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="parent")
	private Set<Folder> children = new HashSet<Folder>();
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="folder")
	private Set<Rule> folderRules = new HashSet<Rule>();
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="folder")
	private Set<Message> folderMessages = new HashSet<Message>();
	
	@ManyToOne
	@JoinColumn(name="account_id", referencedColumnName="a_id", nullable=false)
	private Account account;
	
	public Folder() {
		
	}

	public Folder(long id, String name, Folder folder, Set<Folder> folderFolders,
			Set<Rule> folderRules, Set<Message> folderMessages, Account account) {
		super();
		this.id = id;
		this.name = name;
		this.parent = folder;
		this.children = folderFolders;
		this.folderRules = folderRules;
		this.folderMessages = folderMessages;
		this.account = account;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Folder getFolder() {
		return parent;
	}

	public void setFolder(Folder folder) {
		this.parent = folder;
	}

	public Set<Folder> getFolderFolders() {
		return children;
	}

	public void setFolderFolders(Set<Folder> folderFolders) {
		this.children = folderFolders;
	}

	public Set<Rule> getFolderRules() {
		return folderRules;
	}

	public void setFolderRules(Set<Rule> folderRules) {
		this.folderRules = folderRules;
	}

	public Set<Message> getFolderMessages() {
		return folderMessages;
	}

	public void setFolderMessages(Set<Message> folderMessages) {
		this.folderMessages = folderMessages;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
