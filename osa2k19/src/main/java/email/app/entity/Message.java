package email.app.entity;

import static javax.persistence.CascadeType.ALL;


import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="messages")
public class Message implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="m_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="m_from", unique=false, nullable=false) 
	private String from;
	
	@Column(name="m_to", unique=false, nullable=false) 
	private String to;
	
	@Column(name="m_cc", unique=false, nullable=true) 
	private String cc;
	
	@Column(name="m_bcc", unique=false, nullable=true) 
	private String bcc;
	
	@Column(name="m_dateTime", unique=false, nullable=false) 
	private Date dateTime;
	
	@Column(name="m_subject", unique=false, nullable=false) 
	private String subject;
	
	@Column(name="m_content", unique=false, nullable=false) 
	private String content;
	
	@Column(name="m_unread", unique=false, nullable=false) 
	private boolean unread;
	
	@ManyToOne
	@JoinColumn(name="account_id", referencedColumnName="a_id", nullable=false)
	private Account account;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="message")
	private Set<Attachment> messageAttachments = new HashSet<Attachment>();
	
	@ManyToMany
	@JoinTable(name="messages_tags", joinColumns= {@JoinColumn(name="message_id")},
	inverseJoinColumns= {@JoinColumn(name="tag_id")})
	private Set<Tag> tags = new HashSet<Tag>();
	
	@ManyToOne
	@JoinColumn(name="folder_id", referencedColumnName="f_id", nullable=false)
	private Folder folder;
	
	public Message() {
		
	}
	
	public Message(Message m) {
		
	}

	public Message(long id, String from, String to, String cc, String bcc, Date dateTime, String subject,
			String content, boolean unread, Account account, Folder folder, Set<Attachment> messageAttachments,
			Set<Tag> messageTags) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.unread = unread;
		this.account = account;
		this.messageAttachments = messageAttachments;
		this.tags = messageTags;
		this.folder = folder;
	}
	
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Set<Attachment> getMessageAttachments() {
		return messageAttachments;
	}

	public void setMessageAttachments(Set<Attachment> messageAttachments) {
		this.messageAttachments = messageAttachments;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> messageTags) {
		this.tags = messageTags;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}
}
