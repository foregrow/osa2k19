package email.app.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="photos")
public class Photo implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="p_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="p_path", unique=true, nullable=false) 
	private String path;
	
	@ManyToOne
	@JoinColumn(name="contact_id", referencedColumnName="c_id", nullable=true)
	private Contact contact;
	
	public Photo() {
		
	}

	public Photo(long id, String path, Contact contact) {
		super();
		this.id = id;
		this.path = path;
		this.contact = contact;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
}
