package email.app.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import email.app.dto.Condition;
import email.app.dto.Operation;

@SuppressWarnings("serial")
@Entity
@Table(name="rules")
public class Rule implements Serializable{

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="r_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="r_condition", unique=true, nullable=false) 
	private Condition condition;
	
	@Column(name="r_operation", unique=true, nullable=false) 
	private Operation operation;
	
	@Column(name="r_value", unique=true, nullable=false) 
	private String value;
	
	@ManyToOne
	@JoinColumn(name="folder_id", referencedColumnName="f_id", nullable=false)
	private Folder folder;
	
	public Rule() {
		
	}

	public Rule(long id, Condition condition, Operation operation, String value, Folder folder) {
		super();
		this.id = id;
		this.condition = condition;
		this.operation = operation;
		this.value = value;
		this.folder = folder;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}
	
	
}
