package email.app.entity;

import static javax.persistence.GenerationType.IDENTITY;


import java.io.Serializable;
import java.util.HashSet;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="tags")
public class Tag implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="t_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="t_name", unique=true, nullable=false) 
	private String name;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="u_id", nullable=false)
	private User user;
	
	@ManyToMany(mappedBy="tags")
	private Set<Message> messages = new HashSet<Message>();
	
	public Tag() {
		
	}

	public Tag(long id, String name, User user, Set<Message> tagMessages) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
		this.messages = tagMessages;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> tagMessages) {
		this.messages = tagMessages;
	}
	
	
}
