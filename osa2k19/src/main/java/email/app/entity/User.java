package email.app.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="users")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="u_id", unique=true, nullable=false) 
	private long id;
	
	@Column(name="u_username", unique=true, nullable=false) 
	private String username;
	
	@Column(name="u_password", unique=false, nullable=false) 
	private String password;
	
	@Column(name="u_firstName", unique=false, nullable=false) 
	private String firstName;
	
	@Column(name="u_lastName", unique=false, nullable=false) 
	private String lastName;
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="user")
	private Set<Account> userAccounts = new HashSet<Account>();
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="user")
	private Set<Tag> userTags = new HashSet<Tag>();
	
	@OneToMany(cascade={ALL}, fetch=LAZY, mappedBy="user")
	private Set<Contact> userContacts = new HashSet<Contact>();
	
	public User() {
		
	}

	public User(long id, String username, String password, String firstName, String lastName,
			Set<Account> userAccounts, Set<Tag> userTags, Set<Contact> userContacts) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userAccounts = userAccounts;
		this.userTags = userTags;
		this.userContacts = userContacts;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Account> getUserAccounts() {
		return userAccounts;
	}

	public void setUserAccounts(Set<Account> userAccounts) {
		this.userAccounts = userAccounts;
	}

	public Set<Tag> getUserTags() {
		return userTags;
	}

	public void setUserTags(Set<Tag> userTags) {
		this.userTags = userTags;
	}

	public Set<Contact> getUserContacts() {
		return userContacts;
	}

	public void setUserContacts(Set<Contact> userContacts) {
		this.userContacts = userContacts;
	}
	
	
}
