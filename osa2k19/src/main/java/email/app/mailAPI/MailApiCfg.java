package email.app.mailAPI;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;


import org.springframework.stereotype.Component;

import email.app.entity.Account;


@Component
public class MailApiCfg {
	

	
	public static email.app.entity.Message sendEmail(String from, String to, String cc, String bcc, String hostAdr, String hostPort, 
			String senderPassword, String subject, String content) {
		
		final String uname = from;
	    final String pass = senderPassword;
	    
	    Properties propvls = new Properties();
	    propvls.put("mail.smtp.auth", "true");
	    propvls.put("mail.smtp.starttls.enable", "true");
	    propvls.put("mail.smtp.host", hostAdr);
	    propvls.put("mail.smtp.port", hostPort);
	    
	    @SuppressWarnings("unused")
		Session session = Session.getDefaultInstance(propvls);
	     
	      Session sessionobj = Session.getInstance(propvls, new Authenticator() { 
	    	  	@Override
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(uname, pass);
		   }
	         });
	      
	      try {
	   	   Message messageobj = new MimeMessage(sessionobj);
	   	   messageobj.setFrom(new InternetAddress(from));
	   	   messageobj.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
	   	   if(!cc.equals("") && cc != null) {
	   		 messageobj.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
	   	   }
	   	   if(!bcc.equals("") && bcc != null) {
	   		 messageobj.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
	   	   }
	   	   messageobj.setSubject(subject);
	   	   messageobj.setText(content);
	   	   Transport.send(messageobj);
	   	   System.out.println("Your email sent successfully....");
	   	   
	   	   return null;
	         } catch (MessagingException exp) {
	            throw new RuntimeException(exp);
	         }
	}
	
	public static List<email.app.entity.Message> loadEmails(Account acc, email.app.entity.Folder folder) {
		List<email.app.entity.Message> all = new ArrayList<>();
		
		try {
		      Properties propvals = new Properties();
		      propvals.put("mail.imap.host", acc.getInServerAddress());
		      propvals.put("mail.imap.port", acc.getInServerPort());
		      propvals.put("mail.imap.starttls.enable", "true");
		      Session emailSessionObj = Session.getDefaultInstance(propvals);  

		      Store storeObj = emailSessionObj.getStore("imaps");
		      storeObj.connect(acc.getInServerAddress(), acc.getUsername(), acc.getPassword());
		      
		      Folder inbox = storeObj.getFolder("INBOX");
		      inbox.open(Folder.READ_ONLY);
		      
		      Flags re = new Flags(Flags.Flag.SEEN);
		      FlagTerm unseenFlagTerm = new FlagTerm(re, false);
		      Message[] messageobjs = inbox.search(unseenFlagTerm);
		      if (messageobjs.length > 0) {
		    	  for(Message mess : messageobjs) {
			    	  
			    	  	
						StringBuilder sb = new StringBuilder();

						for (int i = 0; i < mess.getRecipients(Message.RecipientType.TO).length; i++) {
							if (i != 0) {
								sb.append(",");
							}
							sb.append(mess.getRecipients(Message.RecipientType.TO)[i].toString());
						}
						
						email.app.entity.Message ent = new email.app.entity.Message();
				    	
						ent.setFrom(mess.getFrom()[0].toString());  
						ent.setTo(sb.toString());
						sb.setLength(0);
						ent.setCc("");
						ent.setBcc("");
						ent.setDateTime(mess.getSentDate());
						ent.setSubject(mess.getSubject());
						ent.setUnread(true);
						ent.setFolder(folder);
						ent.setAccount(acc);
						
						String result = "";
						if (mess.isMimeType("text/plain")) {
					        result = mess.getContent().toString();
					    } else if (mess.isMimeType("multipart/*")) {
					        MimeMultipart mimeMultipart = (MimeMultipart) mess.getContent();
					        result = getTextFromMimeMultipart(mimeMultipart);
					        
					    }
						ent.setContent(result);
						all.add(ent);
			      }
		      }
		      
		 
		      
		      inbox.close(false);
		      storeObj.close();
		      
		      } catch (NoSuchProviderException exp) {
		         exp.printStackTrace();
		      } catch (MessagingException exp) {
		         exp.printStackTrace();
		      } catch (Exception exp) {
		         exp.printStackTrace();
		      }
		
		return all;
	}
	
	private static String getTextFromMimeMultipart(
	        MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; // without break same text appears twice in my tests
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
	}
}
