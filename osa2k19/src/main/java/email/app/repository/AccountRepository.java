package email.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import email.app.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findAccountByUsername(String username);
	
	Account findAccountById(long id);
	
	void deleteById(long id);
}
