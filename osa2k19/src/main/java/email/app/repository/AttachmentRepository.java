package email.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import email.app.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

}
