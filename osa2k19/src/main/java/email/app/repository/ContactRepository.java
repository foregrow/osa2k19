package email.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;


import email.app.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {

	Contact findContactById(long id);

	void deleteById(long id);
}
