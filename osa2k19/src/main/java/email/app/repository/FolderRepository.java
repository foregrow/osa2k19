package email.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import email.app.entity.Account;
import email.app.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, Long> {

	Folder findFolderById(long id);
	
	void deleteById(long id);
	
	Folder findByNameAndAccount(String name, Account account);
	
}
