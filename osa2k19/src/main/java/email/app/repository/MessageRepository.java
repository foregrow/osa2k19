package email.app.repository;

import java.util.Date;


import org.springframework.data.jpa.repository.JpaRepository;


import email.app.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

	Message findByDateTimeAndAccountIdAndFolderId(Date dateTime, long id, long fid);
	
	Message findById(long id);
	
}
