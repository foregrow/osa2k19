package email.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import email.app.entity.Photo;


public interface PhotoRepository extends JpaRepository<Photo, Long> {

}
