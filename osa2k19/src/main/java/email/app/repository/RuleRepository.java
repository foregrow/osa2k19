package email.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import email.app.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Long> {

}
