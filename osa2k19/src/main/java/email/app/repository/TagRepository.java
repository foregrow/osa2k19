package email.app.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import email.app.entity.Tag;
import email.app.entity.User;

public interface TagRepository extends JpaRepository<Tag, Long> {

	Tag findTagById(long id);

	void deleteById(long id);
	
	Tag findByUserAndName(User user, String tagName);
	
	List<Tag> findAllByUser(User user);
	
}
