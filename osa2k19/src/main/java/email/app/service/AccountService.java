package email.app.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Account;
import email.app.repository.AccountRepository;

@Service
public class AccountService implements AccountServiceInterface{
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public List<Account> findAll(){
		return accountRepository.findAll();
	}
	
	@Override
	public void save(Account account){
		accountRepository.save(account);
	}

	@Override
	public Account findAccountByUsername(String username) {
		return accountRepository.findAccountByUsername(username);
	}

	@Override
	public Account findAccountById(long id) {
		return accountRepository.findAccountById(id);
	}
	
	@Override
	public void delete(long id) {
		accountRepository.delete(id);
	}
	
}
