package email.app.service;

import java.util.List;

import email.app.entity.Account;

public interface AccountServiceInterface {

	List<Account> findAll();
	
	void save(Account account);
	
	Account findAccountByUsername(String username);
	
	Account findAccountById(long id);
	
	void delete(long id);
	
	
	
}
