package email.app.service;

import java.util.List;


import email.app.entity.Contact;

public interface ContactInterface {

	List<Contact> findAll();
	
	void save(Contact c);
	
	Contact findContactById(long id);
	
	void delete(long id);
	
}
