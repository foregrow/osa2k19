package email.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Contact;
import email.app.repository.ContactRepository;

@Service
public class ContactService implements ContactInterface {
	
	@Autowired
	private ContactRepository contactRepository;

	@Override
	public List<Contact> findAll() {
		
		return contactRepository.findAll();
	}

	@Override
	public void save(Contact c) {
		contactRepository.save(c);
		
	}

	@Override
	public Contact findContactById(long id) {
		return contactRepository.findContactById(id);
	}

	@Override
	public void delete(long id) {
		contactRepository.delete(id);
		
	}

}
