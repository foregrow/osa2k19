package email.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Account;
import email.app.entity.Folder;
import email.app.repository.FolderRepository;

@Service
public class FolderService implements FolderServiceInterface {

	@Autowired
	private FolderRepository folderRepository;
	
	@Override
	public List<Folder> findAll(){
		return folderRepository.findAll();
	}

	@Override
	public Folder findFolderById(long id) {
		return folderRepository.findFolderById(id);
	}

	@Override
	public void save(Folder folder) {
		folderRepository.save(folder);
		
	}
	
	@Override
	public void delete(long id) {
		folderRepository.delete(id);
	}

	@Override
	public Folder findByNameAndAccount(String name, Account account) {
		return folderRepository.findByNameAndAccount(name, account);
	}
	
}
