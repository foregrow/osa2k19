package email.app.service;

import java.util.List;


import email.app.entity.Account;
import email.app.entity.Folder;

public interface FolderServiceInterface {

	List<Folder> findAll();
	
	Folder findFolderById(long id);
	
	void save(Folder folder);
	
	void delete(long id);
	
	Folder findByNameAndAccount(String name, Account account);
	
}
