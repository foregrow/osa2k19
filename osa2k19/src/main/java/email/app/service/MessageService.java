package email.app.service;

import java.util.Date;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Message;
import email.app.repository.MessageRepository;

@Service
public class MessageService implements MessageServiceInterface {

	@Autowired
	MessageRepository messageRep;
	@Override
	public List<Message> findAll() {
		return messageRep.findAll();
	}
	@Override
	public void save(Message message) {
		messageRep.save(message);
		
	}
	@Override
	public Message findByDateTimeAndAccountIdAndFolderId(Date dateTime, long id, long fid) {
		return messageRep.findByDateTimeAndAccountIdAndFolderId(dateTime, id, fid);
	}
	@Override
	public Message findById(long id) {
		
		return messageRep.findById(id);
	}
	


}
