package email.app.service;

import java.util.Date;

import java.util.List;

import email.app.entity.Message;

public interface MessageServiceInterface {

	List<Message> findAll();
	
	void save(Message message);
	
	Message findByDateTimeAndAccountIdAndFolderId(Date dateTime, long id, long fid);
	
	Message findById(long id);
	
}
