package email.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Photo;
import email.app.repository.PhotoRepository;

@Service
public class PhotoService implements PhotoServiceInterface {

	@Autowired
	PhotoRepository photoRepository;
	
	@Override
	public void save(Photo photo) {
		photoRepository.save(photo);	
	}

	@Override
	public List<Photo> findAll() {
		return photoRepository.findAll();
	}

}
