package email.app.service;

import java.util.List;

import email.app.entity.Photo;

public interface PhotoServiceInterface {

	void save(Photo photo);
	
	List<Photo> findAll();
}
