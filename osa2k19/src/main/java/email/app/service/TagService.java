package email.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import email.app.entity.Tag;
import email.app.entity.User;
import email.app.repository.TagRepository;

@Service
public class TagService implements TagServiceInterface {

	@Autowired
	private TagRepository tagRepository;
	
	@Override
	public List<Tag> findAll(){
		return tagRepository.findAll();
	}

	@Override
	public Tag findTagById(long id) {
		return tagRepository.findTagById(id);
	}

	@Override
	public void save(Tag tag) {
		tagRepository.save(tag);
		
	}

	@Override
	public void delete(long id) {
		tagRepository.delete(id);
	}

	@Override
	public Tag findByUserAndName(User user, String tagName) {
		return tagRepository.findByUserAndName(user, tagName);
	}
	
	@Override
	public List<Tag> findAllByUser(User user) {
		return tagRepository.findAllByUser(user);
	}

	
	
}
