package email.app.service;

import java.util.List;

import email.app.entity.Tag;
import email.app.entity.User;

public interface TagServiceInterface {

	List<Tag> findAll();
	
	Tag findTagById(long id);
	
	void save(Tag tag);
	
	void delete(long id);
	
	Tag findByUserAndName(User user, String tagName);
	
	List<Tag> findAllByUser(User user);
	
}
