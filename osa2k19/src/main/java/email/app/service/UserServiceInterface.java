package email.app.service;

import java.util.List;


import email.app.entity.User;



public interface UserServiceInterface {

	List<User> findAll();
	
	User findByUsernameAndPassword(String username, String password);
	
	void save(User user);
	
	User findByUsername(String username);
	
	
	
}
