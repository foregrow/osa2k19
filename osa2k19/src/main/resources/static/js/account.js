$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-2];
	
	
	$('#profile').append('<a href="/users/accounts/' + id +'/profile">Profile</a>');
	$('#folders').append('<a href="/users/accounts/' + id +'/folders">Folders</a>');
	

	var sort = -1;
	var search = 'prazan';
	var searchInt = 0;
	var tagSearch = 'prazan';
	var tagSearchInt = 0;
	function load(){
		$.get('/users/accounts/messages/view',{'id':id, 'sort': sort,'search':search,'searchInt':searchInt,'tagSearch':tagSearch,'tagSearchInt':tagSearchInt}, function(mdto){
			$('#messagesTable').find('tr:gt(2)').remove();
			
			for(it in mdto) {
				
				var d = new Date(mdto[it].dateTime);
				var date = d.toLocaleString();
				if(mdto[it].unread){
					$('#messagesTable').append('<tr bgcolor="#FF0000">' + 
							'<td><a href="/users/accounts/' + id + '/messages/' + mdto[it].id + '">' + date + '</a></td>' + 
							'<td>' + mdto[it].from + '</td>' + 
							'<td>' + mdto[it].to + '</td>' + 
							'<td>' + mdto[it].subject + '</td>' + 
					'</tr>');
				}else{
					$('#messagesTable').append('<tr>' + 
							'<td><a href="/users/accounts/' + id + '/messages/' + mdto[it].id + '">' + date + '</a></td>' + 
							'<td>' + mdto[it].from + '</td>' + 
							'<td>' + mdto[it].to + '</td>' + 
							'<td>' + mdto[it].subject + '</td>' + 
					'</tr>');
				}
				
			}
			$('#messagesTable').append('<tr bgcolor="lightgrey" ><td colspan="4"> <button type="submit" id="sendMessage" style="height:100%;width:100%">Create new message</button> </td></tr>');
			$("#sendMessage").on('click', function(event) {
				window.location.replace("/users/accounts/" + id +"/messages/createMessageView");
			});	
		});
	}
	
	load();
	
	$("#dateTimeAsc").on('click', function(event) {
		sort = 1;
		load();
		
		});	
	$("#dateTimeDesc").on('click', function(event) {
		sort = 2;
		load();	
		});	
	$("#fromAsc").on('click', function(event) {
		sort = 3;
		load();
		
		
		});	
	$("#fromDesc").on('click', function(event) {
		sort = 4;
		load();
		

		});	
	$("#subjectAsc").on('click', function(event) {
		sort = 5;
		load();

		});	
	$("#subjectDesc").on('click', function(event) {
		sort = 6;
		load();
		});	
	
	
	$('#searchMessages').on('keyup', function(event) {
		search = $('#searchMessages').val();
		console.log(search + "= search");
		searchInt = 1;
		load();
	
	});

	$('#searchTags').on('keyup', function(event) {
		tagSearch = $('#searchTags').val();
		console.log(tagSearch + "= tagsearch");
		tagSearchInt = 1;
		load();
	
	});
	
});