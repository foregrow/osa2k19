$(document).ready(function(){
	var passwordInput = $('#passwordInput');
	
	var url_string = document.URL;
	var id = url_string.slice(-1);
	$('#loginSubmit').on('click', function(event) { 
		var password = passwordInput.val();
		
		var params = {
				'id': id,
				'password': password
			}
		
		$.get('/users/accounts/loginView', params, function(obavestenje) { 
			
			if(obavestenje == "pBad"){
				alert('Wrong password! ');
				passwordInput.val('');
				return;
			}
			
			window.location.replace('/users/accounts/'+ id + '/messages');
				
		});
		
		
	});
	
});