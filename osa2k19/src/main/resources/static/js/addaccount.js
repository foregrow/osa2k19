$(document).ready(function(){
	
	$("#addSubmit").on('click', function(event) {
		
		var displayName = $('#displayNameInput').val();
		var userName = $('#userNameInput').val();
		var password = $('#passwordInput').val();
		
		params={
			'displayName':displayName,	
			'userName':userName,
			'password':password	
		};
		
		$.post('/users/accounts/add', params, function(obavestenje) { 
			if(obavestenje == "uNull"){
				alert("Username can't be empty! ");
				return;				
			}else if(obavestenje == "dNull"){
				alert("Display name can't be empty! ");
				return;				
			}else if(obavestenje == "pNull"){
				alert("Password can't be empty! ");
				return;				
			}else if(obavestenje == "aFound"){
				alert("Username already exists! ");
				return;	
			}
			
			window.location.replace('/home');
		});
		event.preventDefault();
		return false;
	});
});