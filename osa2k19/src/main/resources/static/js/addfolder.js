$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-3];
	console.log(id);	
	$("#addSubmit").on('click', function(event) {
		
		var name = $('#nameInput').val();
		var parent = $('#parentInput').val();
		
		params={
				'name':name,	
				'parent':parent,	
				'id':id
			};
		
		$.post('/users/accounts/folders/add', params, function(obavestenje) { 
			if(obavestenje == "nNull"){
				alert("Name can't be empty! ");
				return;				
			}else if(obavestenje == "pNull"){
				alert("Parent name does not exist! ");
				return;				
			}else if(obavestenje == "isNull"){
				alert("Cannot duplicate entered folder! ");
				return;				
			}else if(obavestenje == "pisNull"){
				alert("Cannot add entered folder as parent! ");
				return;				
			}
			
			window.location.replace('/users/accounts/'+ id +'/folders');
		});
		event.preventDefault();
		return false;
	});
		
		
});