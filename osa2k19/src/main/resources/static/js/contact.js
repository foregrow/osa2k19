$(document).ready(function(){
	
	var url_string = document.URL;
	var id = url_string.slice(-1);
	$.get('/users/contacts/view',{'id':id}, function(dto){
		console.log(dto.contactPhotosDTO);
		var photosDTO = dto.contactPhotosDTO;
		for(it in photosDTO){
			var path = photosDTO[it].path;
			var photoNames = path.split('/');
			var photoName = photoNames[photoNames.length-1];
			$('#photoForm').append("<img src='chrome-extension://opnkbfhodeffnhjcjmjdhoclejfofkjh/"+ id + '/' + photoName +"' height='80' width='80' title='tt' alt='alt'>");
		}
		$('#displayNameCell').text(dto.displayName);
		$('#emailCell').text(dto.email);
		$('#firstNameCell').text(dto.firstName);
		$('#lastNameCell').text(dto.lastName);
		$('#noteCell').text(dto.note);
		
		$("#displayNameInput").val(dto.displayName);
		$("#emailInput").val(dto.email);
		$("#firstNameInput").val(dto.firstName);
		$("#lastNameInput").val(dto.lastName);
		$("#noteInput").val(dto.note);		
	});
	
	$("#updateSubmit").on('click', function(event) {
		var displayName = $('#displayNameInput').val();
		var email = $('#emailInput').val();
		var firstName = $('#firstNameInput').val();
		var lastName = $('#lastNameInput').val();
		var note = $('#noteInput').val();
		
		params={
				'id': id,
				'displayName':displayName,
				'email':email,
				'firstName':firstName,
				'lastName':lastName,
				'note':note
		}
		
		$.ajax({
		    url: '/users/contacts/update',
		    type: 'PUT',
		    data: params,
		    success: function(obavestenje) {
		    	if(obavestenje == 'dNull'){
					alert("Display name can't be empty! ");
					return;
				}else if(obavestenje == 'eNull'){
					alert("Email can't be empty! ");
					return;
				}
				window.location.replace('/users/contacts/'+id);
		    }
		
		});
		event.preventDefault();
		return false;
	});
	

	$("#deleteSubmit").on('click', function(event) {
		
		$.ajax({
		    url: '/users/contacts/delete/'+id,
		    type: 'DELETE',
		    success: function(result) {
		    	
		    	alert("Contact is deleted! ");
				
				window.location.replace('/home');
		    }
		
		});
		event.preventDefault();
		return false;
	});
	
	
	$("#updateForm").append(
			"<form action='/users/contacts/uploadPhoto/" + id + "' method='post' enctype='multipart/form-data'>" +
			"<input type='file' name='files' multiple='multiple'/>" +
			"<input type='submit' value='Upload Files'/>" +
		"</form>");
	
	
	
	
});	