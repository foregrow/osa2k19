$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-1];
	var id1 = parts[parts.length-3];

	$('#back').append('<a href="/users/accounts/' + id1 +'/folders">Back</a>');
	
	$("#updateSubmit").on('click', function(event) {

		var name = $('#nameInput').val();
		
		params={
				'id': id,
				'name':name
		}
		
		$.ajax({
		    url: '/users/accounts/folders/update',
		    type: 'PUT',
		    data: params,
		    success: function(obavestenje) {
		    	if(obavestenje == 'nNull'){
					alert("Name can't be empty! ");
					return;
				}
				window.location.replace('/users/accounts/'+id1 + '/folders');
		    }
		
		});
		event.preventDefault();
		return false;
	});
	
	$("#deleteSubmit").on('click', function(event) {
		
		$.ajax({
		    url: '/users/accounts/folders/delete/'+id,
		    type: 'DELETE',
		    success: function(result) {
		    	
		    	alert("Folder is deleted! ");
				
				window.location.replace('/home');
		    }
		
		});
		event.preventDefault();
		return false;
	});
});