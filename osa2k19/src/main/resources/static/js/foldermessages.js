$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-2];
	var aid = parts[parts.length-4];
	
	$.get('/users/accounts/folders/messages',{'id':id}, function(dto){
		var messages = dto.folderMessagesDTO;
		var children = dto.folderFoldersDTO;
		console.log(children);
		for (it in children){
			$('#childTable').append('<tr bgcolor=>' + 
					'<td>' + children[it].name+ '</td>' + 
			'</tr>');
		}
		for(it in messages) {
			var d = new Date(messages[it].dateTime);
			var date = d.toLocaleString();
			if(messages[it].unread){
				$('#messagesTable').append('<tr bgcolor="#FF0000">' + 
						'<td><a href="/users/accounts/' + aid + '/messages/' + messages[it].id + '">' + date + '</a></td>' + 
						'<td>' + messages[it].from + '</td>' + 
						'<td>' + messages[it].to + '</td>' + 
						'<td>' + messages[it].subject + '</td>' + 
				'</tr>');
			}else{
				$('#messagesTable').append('<tr>' + 
						'<td><a href="/users/accounts/' + aid + '/messages/' + messages[it].id + '">' + date + '</a></td>' + 
						'<td>' + messages[it].from + '</td>' + 
						'<td>' + messages[it].to + '</td>' + 
						'<td>' + messages[it].subject + '</td>' + 
				'</tr>');
			}
			
		}
		});
	});