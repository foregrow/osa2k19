$(document).ready(function(){
	var userNameInput = $('#userNameInput');
	var passwordInput = $('#passwordInput');
	
	
	$('#loginSubmit').on('click', function(event) { 
		var userName = userNameInput.val();
		var password = passwordInput.val();
		
		var params = {
				'userName': userName, 
				'password': password
			}
		
		$.post('/authenticate', params, function(user) { 
			
			if(user.username!=userName || user.password!=password){
				alert('Korisnicko ime i lozinka se ne slazu! ');
				userNameInput.val('');
				passwordInput.val('');
				return;
			}else{
				
				window.location.replace('/home');
			}
				
		});
		
		
	});
	
});