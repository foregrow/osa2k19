$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var aid = parts[parts.length-3];
	var id = parts[parts.length-1];
	
	
	$('#back').append('<a href="/users/accounts/' + aid +'/messages">Back</a>');
	
	$.get('/users/accounts/'+ aid+ '/messages/messageView',{'id':id}, function(dto){
		var tags = dto.tags;
		var tagNames = "";
		for(it in tags){
			tagNames += tags[it].name + ",";
		}
		$('#fromCell').text(dto.from);
		$('#toCell').text(dto.to);
		$('#subjectCell').text(dto.subject);
		$('#contentCell').text(dto.content);
		$('#tags').text(tagNames);
	});
	
	$("#addSubmit").on('click', function(event) {
		var tagName = $('#tagInput').val();
		$.post('/users/accounts/'+ aid+ '/messages/addTag',{'id':id,'tagName':tagName}, function(obavestenje){
			if(obavestenje == 'mNull'){
				alert('Invalid message! ');
				return;
			}else if(obavestenje == 'tNull'){
				alert('Tag name does not exist! ');
				return;
			}
			alert('Added! ');
			window.location.replace('/users/accounts/'+ aid+ '/messages/'+id);
		});
		event.preventDefault();
		return false;
	});
	
	$("#addSubmitF").on('click', function(event) {
		var folderName = $('#folderInput').val();
		$.post('/users/accounts/'+ aid+ '/messages/addFolder',{'id':id,'aid':aid,'folderName':folderName}, function(obavestenje){
			if(obavestenje == 'mNull'){
				alert('Invalid message! ');
				return;
			}else if(obavestenje == 'fNull'){
				alert('You must enter folder name! ');
				return;
			}else if(obavestenje == 'isNull'){
				alert('Folder was not found! ');
				return;
			}else if(obavestenje == 'afNull'){
				alert('Folder was not found! ');
				return;
			}
			alert('Message added to specified folder! ');
			window.location.replace('/users/accounts/'+ aid+ '/messages/'+id);
		});
		event.preventDefault();
		return false;
	});
});