$(document).ready(function(){
	
	jQuery.extend({
	    put: function(url, data, callback, type) {
	        return _ajax_request(url, data, callback, type, 'PUT');
	    },
	    delete_: function(url, data, callback, type) {
	        return _ajax_request(url, data, callback, type, 'DELETE');
	    }
	});
	
	var firstNameInput = $('#firstNameInput');
	var lastNameInput = $('#lastNameInput');
	var userNameInput = $('#userNameInput');
	var passwordInput = $('#passwordInput');
	
	$.get('/users/profileView', function(dto){
		$('#firstNameCell').text(dto.firstName);
		$('#lastNameCell').text(dto.lastName);
		$('#userNameCell').text(dto.username);
		$('#passwordCell').text(dto.password);
		
		firstNameInput.val(dto.firstName);
		lastNameInput.val(dto.lastName);
		userNameInput.val(dto.username);
		passwordInput.val(dto.password);
		
	});
	
	$('#updateSubmit').on('click', function(event) {
		var userName = $('#userNameInput').val();
		var password = $('#passwordInput').val();
		var firstName = $('#firstNameInput').val();
		var lastName = $('#lastNameInput').val();
		console.log('userName'+ userName);
		console.log('password'+ password);
		console.log('lastName'+ lastName);
		console.log('firstName'+ firstName);
		
		params = {
				'userName': userName,
				'password': password,
				'firstName': firstName,
				'lastName': lastName
		};
		
		$.ajax({
		    url: '/users/updateProfile',
		    type: 'PUT',
		    data: params,
		    success: function(obavestenje) {
		    	if(obavestenje == 'uPostoji'){
					alert('User name already exists! ');
					userNameInput.val('');
					return;
				}else if(obavestenje == 'fNull'){
					alert("First name can't be empty! ");
					return;
				}
				else if(obavestenje == 'lNull'){
					alert("Last name can't be empty! ");
					return;
				}
				else if(obavestenje == 'pNull'){
					alert("Password can't be empty! ");
					return;
				}
				else if(obavestenje == 'uNull'){
					alert("User name can't be empty! ");
					return;
				}
				
				window.location.replace('/users/profile');
		    }
		
		});
		event.preventDefault();
		return false;
		
	});
	
	
	
});