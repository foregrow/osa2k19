$(document).ready(function(){
	
	var url_string = document.URL;
	var id = url_string.slice(-1);
	$.get('/users/tags/view',{'id':id}, function(dto){
		$('#nameCell').text(dto.name);
		
		$("#nameInput").val(dto.name);
		
	});
	
	$("#updateSubmit").on('click', function(event) {
		var name = $('#nameInput').val();
		
		params={
				'id': id,
				'name':name
		}
		
		$.ajax({
		    url: '/users/tags/update',
		    type: 'PUT',
		    data: params,
		    success: function(obavestenje) {
		    	if(obavestenje == 'nNull'){
					alert("Name can't be empty! ");
					return;
				}
				window.location.replace('/users/tags/'+id);
		    }
		
		});
		event.preventDefault();
		return false;
	});
	
	
	$("#deleteSubmit").on('click', function(event) {
		
		$.ajax({
		    url: '/users/tags/delete/'+id,
		    type: 'DELETE',
		    success: function(result) {
		    	
		    	alert("Tag is deleted! ");
				
				window.location.replace('/home');
		    }
		
		});
		event.preventDefault();
		return false;
	});
	
});