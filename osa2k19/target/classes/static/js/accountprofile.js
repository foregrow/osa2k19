$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-2];

	$('#back').append('<a href="/users/accounts/' + id +'/messages">Back</a>');
	
	$("#updateSubmit").on('click', function(event) {

		var password = $('#passwordInput').val();
		
		params={
				'id': id,
				'password':password
		}
		
		$.ajax({
		    url: '/users/accounts/update',
		    type: 'PUT',
		    data: params,
		    success: function(obavestenje) {
		    	if(obavestenje == 'pNull'){
					alert("Password can't be empty! ");
					return;
				}
				window.location.replace('/users/accounts/'+id + '/messages');
		    }
		
		});
		event.preventDefault();
		return false;
	});
	
	$("#deleteSubmit").on('click', function(event) {
		
		$.ajax({
		    url: '/users/accounts/delete/'+id,
		    type: 'DELETE',
		    success: function(result) {
		    	
		    	alert("Account is deleted! ");
				
				window.location.replace('/home');
		    }
		
		});
		event.preventDefault();
		return false;
	});
});