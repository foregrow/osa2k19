$(document).ready(function(){
	
	$("#addSubmit").on('click', function(event) {
		
		var displayName = $('#displayNameInput').val();
		var email = $('#emailInput').val();
		var firstName = $('#firstNameInput').val();
		var lastName = $('#lastNameInput').val();
		var note = $('#noteInput').val();
		
		params={
			'displayName':displayName,	
			'email':email,	
			'firstName':firstName,	
			'lastName':lastName,	
			'note':note
		};
		
		$.post('/users/contacts/add', params, function(obavestenje) { 
			if(obavestenje == "eNull"){
				alert("Email can't be empty! ");
				return;				
			}else if(obavestenje == "dNull"){
				alert("Display name can't be empty! ");
				return;				
			}
			
			window.location.replace('/home');
		});
		event.preventDefault();
		return false;
	});
});