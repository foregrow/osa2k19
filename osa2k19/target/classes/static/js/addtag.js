$(document).ready(function(){
	
	$("#addSubmit").on('click', function(event) {
		
		var name = $('#nameInput').val();
		
		params={
			'name':name	
		};
		
		$.post('/users/tags/add', params, function(obavestenje) { 
			if(obavestenje == "nNull"){
				alert("Name can't be empty! ");
				return;				
			}
			
			window.location.replace('/home');
		});
		event.preventDefault();
		return false;
	});
});