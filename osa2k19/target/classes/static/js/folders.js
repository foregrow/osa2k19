$(document).ready(function(){
	
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-2];
	
	$('#add').append('<a href="/users/accounts/' + id +'/folders/addView">Add new folder</a>');
	
	$.get('/users/accounts/folders/view',{'id':id}, function(mdto){
		for(it in mdto) {
			if(mdto[it].name == 'Sent' || mdto[it].name == 'Inbox'){
				$('#messagesTable').append('<tr>' + 
						'<td>' + mdto[it].id + '</a></td>' + 
						'<td><a href="/users/accounts/' + id + '/folders/' + mdto[it].id + '/messages' + '">' + mdto[it].name + '</a></td>' + 
				'</tr>');
			}
			else{
				$('#messagesTable').append('<tr>' + 
						'<td><a href="/users/accounts/' + id + '/folders/' + mdto[it].id + '">' + mdto[it].id + '</a></td>' + 
						'<td><a href="/users/accounts/' + id + '/folders/' + mdto[it].id + '/messages' + '">' + mdto[it].name + '</a></td>' + 
				'</tr>');
			}
		}
		});
	});
