$(document).ready(function(){
	
	
	$('#logoutLink').append('<a href="/login">Log Out</a>');
	$('#profile').append('<a href="/users/profile">Profile</a>');
	
	var contacts = null;
	var accounts = null;
	var tags = null
	
	
	$.get('/users/contacts', function(contactsDTO){
		contacts = contactsDTO;
		for(it in contactsDTO) {

			$('#contactsTable').append('<tr>' + 
					'<td><a href="/users/contacts/' + contactsDTO[it].id + '">' + contactsDTO[it].displayName + '</a></td>' + 
					'<td>' + contactsDTO[it].firstName + '</td>' + 
					'<td>' + contactsDTO[it].lastName + '</td>' + 
					'<td>' + contactsDTO[it].email + '</td>' + 
					'<td>' + contactsDTO[it].note + '</td>' + 
			'</tr>');
		}
		$('#contactsTable').append('<tr bgcolor="lightgrey" ><td colspan="5"> <button type="submit" id="contactAdd" style="height:100%;width:100%">Add</button> </td></tr>');
		$("#contactAdd").on('click', function(event) {
			window.location.replace("/users/contacts/addView");
		});
	});
		
	
	$.get('/users/accounts', function(account) { 
		accounts = account;
		for(it in account){
			if(!account[it].requiredLogin){
				$('#accountsTable').append('<tr>' + 
						'<td><a href="/users/accounts/' + account[it].id + '/messages' + '">' + account[it].username + '</a></td>' + 
						'<td>' + account[it].displayName + '</td>' + 
					'</tr>');
			}else{
				$('#accountsTable').append('<tr>' + 
						'<td><a href="/users/accounts/login/' + account[it].id + '">' + account[it].username + '</a></td>' + 
						'<td>' + account[it].displayName + '</td>' + 
					'</tr>');
			}
			
		}
		$('#accountsTable').append('<tr bgcolor="lightgrey"><td colspan="2"> <button type="submit" id="accountAdd" style="height:100%;width:100%">Add</button> </td></tr>');
		$("#accountAdd").on('click', function(event) {
			window.location.replace("/users/accounts/addView");
		});
	});
	
	$.get('/users/tags',function(tag){
		tags = tag;
		for(it in tag){
			$('#tagsTable').append('<tr>' + 
					'<td><a href="/users/tags/' + tag[it].id + '">' + tag[it].name + '</a></td>' + 
				'</tr>');
		}
		$('#tagsTable').append('<tr bgcolor="lightgrey"><td colspan="1"> <button type="submit" id="tagAdd" style="height:100%;width:100%">Add</button> </td></tr>');
		
		$("#tagAdd").on('click', function(event) {
			window.location.replace("/users/tags/addView");
		});
	});

	
	
	
	
});