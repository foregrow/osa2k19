$(document).ready(function(){
	
	var firstNameInput = $('#firstNameInput');
	var lastNameInput = $('#lastNameInput');
	var userNameInput = $('#userNameInput');
	var passwordInput = $('#passwordInput');
	$('#log').append('<a href="/login">Login page</a>');
	
	
	$('#regSubmit').on('click', function(event) {
		var firstName = firstNameInput.val();
		var lastName = lastNameInput.val();
		var userName = userNameInput.val();
		var password = passwordInput.val();
		
		var params = {
				'firstName': firstName, 
				'lastName': lastName,
				'userName': userName, 
				'password': password
			}
		
		$.post('/reg', params, function(suser) {
			
			if(firstName == "" || lastName == "" || userName == "" || password == ""){
				alert('Sva polja moraju biti popunjena! ');
				return;	
			}else if(suser == "nevalidno"){
				alert('Uneti username vec postoji! ');
				userNameInput.val('');
				return;
			}else{
				alert('Uspesno ste se registrovali! ');
				window.location.replace("/login");
			}
			
		});
	});
});