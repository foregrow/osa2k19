$(document).ready(function(){
	var url = document.URL;
	var parts = url.split('/');
	var id = parts[parts.length-3];
	console.log(id)
	$('#sendSubmit').on('click', function(event) { 
		var to = $('#toInput').val();
		var cc = $('#ccInput').val();
		var bcc = $('#bccInput').val();
		var subject = $('#subjectInput').val();
		var content = $('#contentInput').val();
		
		var params={
				'id':id,
				'to':to,
				'cc':cc,
				'bcc':bcc,
				'subject':subject,
				'content':content
		}
		
		$.post('/users/accounts/messages/createMessage',params, function(obavestenje){
			
			if(obavestenje == 'aNull'){
				alert('Invalid account id! ');
				return;
			}
			
			window.location.replace('/users/accounts/'+id +'/messages');
		});
		event.preventDefault();
		return false;
	});
});